<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\ApiClient;
use App\Http\Controllers\Controller;

class ApiTokenController extends Controller
{ 
    /**
     * Update the authenticated user's API token.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
     
	//create api token
	 public function create_token(Request $request)
    {
		// bt_web55/clx@566
		// print_r($request->cred);die;
		$user=$request->cred['user'];
		$pass=$request->cred['password'];
		if ($this->checkClient($user,$pass )){
           return response()->json(['error' => 'false', 'message' => 'success','body'=>array('token'=>csrf_token())],200);
        }else{
            return response()->json(['error' => 'true', 'message' => 'Invalid request'],200);
        } 
    }
	//check valid cred
	private function checkClient($user_id,$user_pw){
		$user = ApiClient::where(['user_id'=>$user_id,'user_pw'=>md5($user_pw)])->first();
		if($user){
			 ApiClient::where(['user_id'=>$user_id,'user_pw'=>md5($user_pw)])->update(['token' => hash('sha256', csrf_token())]) ;  
			return true;
		}else{
			return false;
		}
	}
}