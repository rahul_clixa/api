<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/createConnection', 'ApiTokenController@create_token');
Route::post('/getData', 'AdmitUserController@create_token');

Route::middleware('api_auth')->get('/user', function (Request $request) {die('sf');
    return $request->user();
});
/* 
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */